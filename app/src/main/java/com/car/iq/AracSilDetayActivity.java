package com.car.iq;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class AracSilDetayActivity extends AppCompatActivity {

    SessionManagement session;
    DatabaseHelper db;
    String plaka;
    EditText plakaedit;
    EditText markaedit;
    EditText modeledit;
    EditText yiledit;
    EditText tsmailedit;
    String sessionPlaka;
    TextView plakaText;
    TextView co2;
    TextView co;
    TextView o2;
    TextView nox;
    TextView tariharaligi;
    boolean esit;
    ScrollView scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arac_sil_detay);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());
        sessionPlaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            plaka = extras.getString("key");
        }

        plakaedit=findViewById(R.id.plaka_edittext);
        markaedit=findViewById(R.id.marka_edittext);
        modeledit=findViewById(R.id.model_edittext);
        yiledit=findViewById(R.id.yil_edittext);
        tsmailedit=findViewById(R.id.tsmail_edittext);
        scrollView=findViewById(R.id.scrollview);
        co2=findViewById(R.id.co2_value);
        o2=findViewById(R.id.o2_value);
        co=findViewById(R.id.co_value);
        nox=findViewById(R.id.nox_value);
        tariharaligi=findViewById(R.id.tarihAraligi);
        plakaText=findViewById(R.id.plaka_value);


        Arac arac=db.aracDetayAl(plaka);

        if(sessionPlaka.equals(plaka)){
            esit=true;

        }
        else{
            esit=false;
        }

        plakaedit.setText(arac.plaka);
        markaedit.setText(arac.marka);
        modeledit.setText(arac.model);
        if(arac.yil!=0){
            yiledit.setText(String.valueOf(arac.yil));
        }
        tsmailedit.setText(arac.tsmail);

        if(db.veriVarMi(plaka)) {
            co2.setText(db.ortalamaAl("co2", plaka));
            o2.setText(db.ortalamaAl("o2", plaka));
            co.setText(db.ortalamaAl("co", plaka));
            nox.setText(db.ortalamaAl("nox", plaka));

            plakaText.setText(plaka);
            tariharaligi.setText(db.tarihAraligiAl(plaka));
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

    }

    public void silButon(View view)
    {

        if(!db.veriVarMi(plaka)){
            db.aracSil("plaka", plaka);
            anasayfa();

        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogStyle);
            builder.setCancelable(true);
            builder.setTitle("Uyarı");
            builder.setMessage("Silmek istediğiniz araca ait ölçüm verileri bulunmaktadır.\nSilmeden önce bu verileri mail olarak almak ister misiniz?");
            builder.setPositiveButton("Hayır",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            db.aracSil("plaka", plaka);
                            dialog.dismiss();
                        }
                    });
            builder.setNegativeButton("Evet",
                    new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        writeToFile(db.mailVeriler(plaka),AracSilDetayActivity.this);
                        DownloadBitMap(scrollView);
                        sendEmail(plaka);
                        db.aracSil("plaka", plaka);
                        dialog.dismiss();
                    }
            });
            builder.setNeutralButton("Vazgeç",
                    new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    anasayfa();
                }
            });


        }
    }

    public void iptalButon(View view){
        this.finish();
    }

    private void sendEmail(String plaka){
        Mail mail =new Mail(this);
        mail.message="Sayın "+session.getUserDetails().get(SessionManagement.KEY_NAME)+"\n"+
plaka+" plakalı aracınızın emisyon verisi ekte EXCEL ile açabileceğiniz şekilde bilginize sunulmuştur. Hızlı bir bilgi edinme amacına yönelik olarak son 30 ölçümün grafiği de ayrıca aşağıda verilmiştir. Bu bilgiyi saklayarak, aracınız ile ilgili bakım işlemlerinde ileride kullanmanız mümkündür.";
        mail.email=session.getUserDetails().get(SessionManagement.KEY_EMAIL);
        mail.subject="BAKAS BILISIM Car-IQ ver.1.0 "+plaka+" plakalı aracın emisyon verisi:";
        mail.execute();
    }

    private void anasayfa(){
        if (esit) {
            session.setPlaka("");
        }
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();


    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("Veriler.csv", Context.MODE_PRIVATE),"windows-1254");
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "Dosya yazması başarısız: " + e.toString());
        }
    }

    public void DownloadBitMap(ScrollView iv)
    {
        ImageGenerator ig=new ImageGenerator();
        ig.downloadData(iv);
        Log.e("callPhone: ", "permission" );
    }
}

