package com.car.iq;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class HakkindaActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hakkinda);

        //activity_hakkinda da ki itemlerin tanımlanması
        Toolbar toolbar=findViewById(R.id.toolbar);
        Button geri_buton=findViewById(R.id.geri_buton);

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);



    }


    //Toolbardaki seçenekler tuşuna basılınca menü çıkması
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }


    //Toolbar menüsünde seçim yapılınca çalışacak kodlar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            //anasayfa seçilince anasayfa activity açılması
            case R.id.anasayfamenu:
                startActivity(new Intent(HakkindaActivity.this, AnasayfaActivity.class));
                return true;

            //fabeacon seçilince fabeacon activity açılması
            case R.id.fabeaconmenu:
                startActivity(new Intent(HakkindaActivity.this, FABeaconActivity.class));
                return true;


            //ihbar seçilince ihbar butonunun bulunduğu anasayfa activity açılması
            case R.id.ihbarmenu:
                startActivity(new Intent(HakkindaActivity.this, AnasayfaActivity.class));
                return true;


        }
        return false;
    }

    //geri tuşuna basılınca hakkında activity kapanması
    public void geri(View view)
    {
            this.finish();
    }



}
