package com.car.iq;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class FABeaconActivity extends AppCompatActivity {

    //Değişken Tanımı
    int battery;
    private static TextView batteryLevel;
    private static TextView currentTime;
    private static Button batteryLevelButton;
    private static Button currentTimeButton;
    private static TextView batteryColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fabeacon);

        //activity_fabeacon da ki itemlerin tanımlanması
        Toolbar toolbar=findViewById(R.id.toolbar);
        batteryLevelButton=findViewById(R.id.batteryLevelButton);
        currentTimeButton=findViewById(R.id.currentTimeButton);
        batteryLevel=findViewById(R.id.batteryLevel);
        currentTime=findViewById(R.id.currentTime);
        batteryColor=findViewById(R.id.batteryColor);

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);


        //Başlangıçta pil ve saat gelmesi
        this.registerReceiver(this.pilOkuyucu, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        this.registerReceiver(this.saatOkuyucu,new IntentFilter(Intent.ACTION_TIME_TICK));
        String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        currentTime.setText(String.valueOf(time));

    }
    //Toolbardaki seçenekler tuşuna basılınca menü çıkması
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }

    //Toolbar menüsünde seçim yapılınca çalışacak kodlar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            //anasayfa seçilince anasayfa activity açılması
            case R.id.anasayfamenu:
                startActivity(new Intent(FABeaconActivity.this, AnasayfaActivity.class));
                return true;


            //ihbar seçilince ihbar butonunun bulunduğu anasayfa activity açılması
            case R.id.ihbarmenu:
                startActivity(new Intent(FABeaconActivity.this, AnasayfaActivity.class));
                return true;


            //hakkında seçilince hakkında activity açılması
            case R.id.hakkinda:
                startActivity(new Intent(FABeaconActivity.this, HakkindaActivity.class));
                return true;
        }
        return false;
    }

    //Pil Seviyesi okuma butonu fonksiyonu
    public void PilOku(View view)
    {
        this.registerReceiver(this.pilOkuyucu, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        Toast.makeText(this,"Pil Okunamadı",Toast.LENGTH_SHORT).show();
    }

    //Saat okuma butonu fonksiyonu
    public void SaatOku(View view)
    {
        String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        currentTime.setText(String.valueOf(time));
        Toast.makeText(this,"Saat Okunamadı",Toast.LENGTH_SHORT).show();
    }

    //Ateşlenme Zamanları aktivitesine yönlendiren fonksiyon
    public void ateslenmeZamanlariButon(View view)
    {
        startActivity(new Intent(FABeaconActivity.this, AteslenmeZamanlariActivity.class));
    }

    //Senkronize et butonu
    public void senkronizasyonButon(View view)
    {
        Toast.makeText(this,"FABeacon a bağlanılamadı",Toast.LENGTH_SHORT).show();
    }

    //Pil okuma fonksiyonu için broadcast listener
    private BroadcastReceiver pilOkuyucu=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            battery = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            batteryLevel.setText("%"+String.valueOf(battery));
            if(battery>=80){
                batteryColor.setBackground(getResources().getDrawable(R.drawable.battery_green));
            }
            else if(battery>=65){
                batteryColor.setBackground(getResources().getDrawable(R.drawable.battery_yellow));
            }
            else {
                batteryColor.setBackground(getResources().getDrawable(R.drawable.battery_red));
            }
        }
    };

    //Saat okuma fonksiyonu için broadcast listener
    private BroadcastReceiver saatOkuyucu=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().compareTo(Intent.ACTION_TIME_TICK)==0)
            {
                String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
                currentTime.setText(String.valueOf(time));
            }
        }
    };

}
