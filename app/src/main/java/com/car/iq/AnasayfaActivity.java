package com.car.iq;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;


public class AnasayfaActivity extends AppCompatActivity {

    ScrollView scrollView;
    TextView seciliArac;
    SessionManagement session;

    DatabaseHelper db;

    //Activity oluşturulduğunda çalışacak komutlar
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);

        //activity_anasayfa da ki itemlerin tanımlanması
        Toolbar toolbar=findViewById(R.id.toolbar);
        seciliArac=findViewById(R.id.secili_arac_text);




        // Session Manager
        session = new SessionManagement(getApplicationContext());
        db=new DatabaseHelper(this);

        if(session.checkPlakaSecildi()){
            HashMap<String,String> plaka=session.getPlaka();
            seciliArac.setText(seciliArac.getText().toString()+plaka.get(SessionManagement.KEY_PLAKA));
        }

        HashMap<String,String> kullanici=session.getUserDetails();

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);


    }



    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }


    public void arac_bilgisi_buton(View view)
    {
        startActivity(new Intent(this, AracBilgisiActivity.class));
    }

    public void cihaz_kontrol_buton(View view)
    {
        startActivity(new Intent(this, CihazKontrolActivity.class));
    }

    public void olcum_yap_buton(View view)
    {
        if(plaka_secili_mi())
        {
            startActivity(new Intent(this, OlcumYapActivity.class));
        }
        else{
            plaka_uyari();
        }
    }

    public void analiz_yap_buton(View view)
    {

        if(plaka_secili_mi())
        {
            if(db.veriVarMi(session.getPlaka().get(SessionManagement.KEY_PLAKA))){
                startActivity(new Intent(this, AnalizYapActivity.class));
            }
            else {
                veri_uyari();
            }
        }
        else{
            plaka_uyari();
        }
    }

    public void kullanici_islemleri_buton(View view)
    {
        startActivity(new Intent(this, KullaniciIslemleriActivity.class));
    }

    public void yardim_buton(View view)
    {

        startActivity(new Intent(this, YardimActivity.class));

    }

    public void tsmail_buton(View view)
    {

        if(plaka_secili_mi())
        {
            startActivity(new Intent(this, TSMailActivity.class));
        }
        else{
            plaka_uyari();
        }
    }

    public boolean plaka_secili_mi()
    {
        if (session.getPlaka().get(SessionManagement.KEY_PLAKA)==""){
            return false;
        }
        else {
            return true;
        }
    }

    public void plaka_uyari(){
        AlertDialog.Builder builder = new AlertDialog.Builder(AnasayfaActivity.this,R.style.AlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Hata");
        builder.setMessage("Seçili bir araç bulunmamaktadır. \nLütfen devam etmeden önce bir araç seçiniz.");
        builder.setNegativeButton("Kapat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void veri_uyari(){
        AlertDialog.Builder builder = new AlertDialog.Builder(AnasayfaActivity.this,R.style.AlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Hata");
        builder.setMessage("Seçili bir araca ait ölçüm bulunmamaktadır. \nLütfen devam etmeden önce bir ölçüm yapınız.");
        builder.setNegativeButton("Kapat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }



}
