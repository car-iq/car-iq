
package com.car.iq;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * GATT veritabanı ile BLE veri bağlantısını yönetme servisi.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP) // lollipop sürüm desteklemesi ve  scan APIs
public class BLEService extends Service {
    private final static String TAG = BLEService.class.getSimpleName();

    // Etkileşimde bulunulacak ve gereken Bluetooth nesneleri
    private static BluetoothManager mBluetoothManager;
    private static BluetoothAdapter mBluetoothAdapter;
    private static BluetoothLeScanner mLEScanner;
    private static BluetoothDevice mLeDevice;
    private static BluetoothGatt mBluetoothGatt;


    public static boolean baglanti=false;

    private static final long SCAN_PERIOD = 10000;
    private Handler handler = new Handler();

    // Intent extras
    public final static String EXTRA_DATA =
            "com.example.ti.bleprojectzero.EXTRA_DATA";
    public final static String EXTRA_LED0 =
            "com.example.ti.bleprojectzero.EXTRA_LED0";
    public final static String EXTRA_LED1 =
            "com.example.ti.bleprojectzero.EXTRA_LED1";
    public final static String EXTRA_BUTTON0 =
            "com.example.ti.bleprojectzero.EXTRA_BUTTON0";
    public final static String EXTRA_BUTTON1 =
            "com.example.ti.bleprojectzero.EXTRA_BUTTON1";


    // Okumamız / yazmamız gereken Bluetooth özellikleri
    private static BluetoothGattCharacteristic mLedCharacterisitc;
    private static BluetoothGattCharacteristic mLed2Characteristic;
    private static BluetoothGattCharacteristic mButtonCharacteristic;
    private static BluetoothGattCharacteristic mButton2Characteristic;
    private static BluetoothGattCharacteristic mWDataCharacteristic;
    private static BluetoothGattCharacteristic mRDataCharacteristic;


    // servis UUID leri
    public static String LED_SERVICE = "F0001110-0451-4000-B000-000000000000";
    public static String BUTTON_SERVICE = "F0001120-0451-4000-B000-000000000000";
    public static String DATA_SERVICE = "F0001130-0451-4000-B000-000000000000";
    public static String LED0_STATE = "F0001111-0451-4000-B000-000000000000";
    public static String LED1_STATE = "F0001112-0451-4000-B000-000000000000";
    public static String BUTTON0_STATE = "F0001121-0451-4000-B000-000000000000";
    public static String BUTTON1_STATE = "F0001122-0451-4000-B000-000000000000";
    public static String STRING_CHAR = "F0001131-0451-4000-B000-000000000000";
    public static String STREAM_CHAR = "F0001132-0451-4000-B000-000000000000";


    // LED durumunu ve CapSense Değerini takip ve Data okuma değişkenleri
    private static boolean mLedSwitchState = false;
    private static boolean mLed2SwitchState = false ;
    private static boolean mButtonSwitchState =false;
    private static boolean mButton2SwitchState =false;
    public static String okunanData="";

    // Ana etkinliğe yapılan yayınlar sırasında kullanılan eylemler
    public final static String ACTION_BLESCAN_CALLBACK =
            "ACTION_BLESCAN_CALLBACK";
    public final static String ACTION_CONNECTED =
            "ACTION_CONNECTED";
    public final static String ACTION_DISCONNECTED =
            "ACTION_DISCONNECTED";
    public final static String ACTION_SERVICES_DISCOVERED =
            "ACTION_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_RECEIVED =
            "ACTION_DATA_RECEIVED";
    public final static String CONNECTION_FAILED =
            "CONNECTION_FAILED";
    public final static String STRING_RECEIVED =
            "STRING_RECEIVED";



    public BLEService() {
    }


    public class LocalBinder extends Binder {
        public BLEService getService() {
            return BLEService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /*@Override
     *//*public boolean onUnbind(Intent intent) {
        // Kaynakları boşaltmak için hizmeti açtığımızda BLE kapatma yöntemi çağrılır.
       *//**//* close();
        return super.onUnbind(intent);*//**//*
    }*/

    private final IBinder mBinder = new LocalBinder();


    public boolean initialize() {

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public void scan() {

        /* Cihazları tarayın ve istediğimiz hizmete sahip olanı arayın */
        UUID capsenseLedService = UUID.fromString(LED_SERVICE);
        UUID[] capsenseLedServiceArray = {capsenseLedService};


        // lollipop versiyon desteği
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //inceleme
            mBluetoothAdapter.startLeScan(capsenseLedServiceArray, mLeScanCallback);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);
        } else { // BLE tarama LOLLIPOP
            ScanSettings settings;
            List<ScanFilter> filters;
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<>();
            // Filtreleme sadece ARAÇ UUID si üzerine tarama
            ParcelUuid PUuid = new ParcelUuid(capsenseLedService);
            ScanFilter filter = new ScanFilter.Builder().setServiceUuid(PUuid).build();
            filters.add(filter);
            mLEScanner.startScan(filters, settings, mScanCallback);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mLEScanner.stopScan(mScanCallback);
                    if(mLeDevice==null){
                        broadcastUpdate(CONNECTION_FAILED);
                    }
                }
            }, SCAN_PERIOD);
        }

    }


    public void connect() {
        if (mBluetoothAdapter == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");//BLT başlatılamadı
            broadcastUpdate(CONNECTION_FAILED);
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothGatt != null) {
            //return mBluetoothGatt.connect();
        }

        // Cihaza doğrudan bağlanmak istiyoruz, bu yüzden autoConnect'i ayarlıyoruz
        // parametresini false olarak ayarladık mümkünse true olacak.
        mBluetoothGatt = mLeDevice.connectGatt(this, false, mGattCallback);


    }

    /**
     *  Bağlı cihazda servis bulmayı çalıştırır.
     */
    public void discoverServices() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.discoverServices();
    }


    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    public void readLedCharacteristic() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(mLedCharacterisitc);

    }

    /**
     * Led durumu 1 0 olmaya bakıyor
     * @param value  LED aç (1) kapa (0)
     */
    public void writeLedCharacteristic(boolean value) {
        byte[] byteVal = new byte[1];
        if (value) {
            byteVal[0] = (byte) (1);
        } else {
            byteVal[0] = (byte) (0);
        }
        Log.i(TAG, "LED " + value);
        mLedSwitchState = value;
        mLedCharacterisitc.setValue(byteVal);
        mBluetoothGatt.writeCharacteristic(mLedCharacterisitc);
    }

    public void readLed2Characteristic(){
        if (mBluetoothAdapter == null || mBluetoothGatt ==null){
            Log.w(TAG,"not");
            return;
        }
        mBluetoothGatt.readCharacteristic(mLed2Characteristic);
    }
    public void writeLed2Characteristic(boolean value){
        byte[] byteVal = new byte[1];
        if (value) {
            byteVal[0] = (byte) (1);
        } else {
            byteVal[0] = (byte) (0);
        }
        Log.i(TAG, "LED 2" + value);
        mLed2SwitchState = value;
        mLed2Characteristic.setValue(byteVal);
        mBluetoothGatt.writeCharacteristic(mLed2Characteristic);
    }


    public String readData() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return "hata";
        }
        mBluetoothGatt.readCharacteristic(mWDataCharacteristic);
        return okunanData;
    }


    public void writeData(String string) {

        String strData = string;
        byte[] data = strData.getBytes();

        mWDataCharacteristic.setValue(data);
        mBluetoothGatt.writeCharacteristic(mWDataCharacteristic);
    }

    public String writeReadData(String string) throws InterruptedException {

        String strData = string;
        byte[] data = strData.getBytes();

        mWDataCharacteristic.setValue(data);
        mBluetoothGatt.writeCharacteristic(mWDataCharacteristic);
        Thread.sleep(250);
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return "hata";
        }
        mBluetoothGatt.readCharacteristic(mWDataCharacteristic);
        Thread.sleep(250);
        return okunanData;

    }

    public void readButtonCharacteristic() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(mButtonCharacteristic);
    }


    public void readButton2Characteristic() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(mButton2Characteristic);
    }


    public boolean getLedSwitchState() {
        return mLedSwitchState;
    }

    public boolean getLed2SwitchState() {
        return  mLed2SwitchState;
    }

    public boolean getButtonSwitchState(){
        return mButtonSwitchState;
    }

    public boolean getButton2SwitchState(){
        return mButton2SwitchState;
    }


    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    mLeDevice = device;
                    if(mLeDevice==null){
                        broadcastUpdate(CONNECTION_FAILED);
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        return;
                    }
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    broadcastUpdate(ACTION_BLESCAN_CALLBACK);
                    connect();
                }
    };

    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            mLeDevice = result.getDevice();
            if (mLeDevice == null){
                Log.e("ScanCallback", "Could not get bluetooth device");
                broadcastUpdate(CONNECTION_FAILED);
                mLEScanner.stopScan(mScanCallback);
                return;
            }
            mLEScanner.stopScan(mScanCallback);
            broadcastUpdate(ACTION_BLESCAN_CALLBACK);
            connect();
        }
    };


    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                broadcastUpdate(ACTION_CONNECTED);
                discoverServices();
                Log.i(TAG, "Connected to GATT server.");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(ACTION_DISCONNECTED);
            }
        }


        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            // Sadece aradığımız hizmeti alın
            BluetoothGattService mLedService = gatt.getService(UUID.fromString(LED_SERVICE));
            BluetoothGattService mButtonService = gatt.getService(UUID.fromString(BUTTON_SERVICE));
            BluetoothGattService mDataService = gatt.getService(UUID.fromString(DATA_SERVICE));

            /* İstediğiniz hizmetten özellikler edinin  */
            mLedCharacterisitc = mLedService.getCharacteristic(UUID.fromString(LED0_STATE));
            mLed2Characteristic = mLedService.getCharacteristic(UUID.fromString(LED1_STATE));
            mButtonCharacteristic = mButtonService.getCharacteristic(UUID.fromString(BUTTON0_STATE));
            mButton2Characteristic = mButtonService.getCharacteristic(UUID.fromString(BUTTON1_STATE));
            mRDataCharacteristic = mDataService.getCharacteristic(UUID.fromString(STREAM_CHAR));
            mWDataCharacteristic = mDataService.getCharacteristic(UUID.fromString(STRING_CHAR));

            //Led ve button durumu okuması
            readLedCharacteristic();
            readLed2Characteristic();
            readButtonCharacteristic();
            readButton2Characteristic();
            baglanti=true;

            // Hizmet / karakteristik / tanımlayıcı keşfinin yapıldığını yayınlayın
            broadcastUpdate(ACTION_SERVICES_DISCOVERED);
        }



        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {


            if (status == BluetoothGatt.GATT_SUCCESS) {
                // Okumanın LED durumu olduğunu doğrulayın
                String uuid = characteristic.getUuid().toString();
                //Bu durumda, uygulamanın yaptığı tek okuma LED durumudur.
                // Uygulamanın okumak için ek özellikleri varsa,
                // burada her biri üzerinde ayrı ayrı çalışmak için bir anahtar deyimi kullanabiliriz.
                if (uuid.equalsIgnoreCase(LED0_STATE)) {
                    final byte[] data = characteristic.getValue();
                    // LED anahtar durumu değişkenini, okunan karakteristik değere göre ayarlandı
                    mLedSwitchState = ((data[0] & 0xff) != 0x00);
                    broadcastUpdate(ACTION_DATA_RECEIVED);
                }
                if (uuid.equalsIgnoreCase(LED1_STATE)){
                    final byte[] data = characteristic.getValue();
                    mLed2SwitchState = ((data[0] & 0xff) != 0x00);
                    broadcastUpdate(ACTION_DATA_RECEIVED);
                }
                if (uuid.equalsIgnoreCase(BUTTON0_STATE)){
                    final byte[] data = characteristic.getValue();

                    mButtonSwitchState = ((data[0] & 0xff) != 0x00);
                    broadcastUpdate(ACTION_DATA_RECEIVED);
                }
                if (uuid.equalsIgnoreCase(BUTTON1_STATE)){
                    final byte[] data = characteristic.getValue();

                    mButton2SwitchState = ((data[0] & 0xff) != 0x00);
                    broadcastUpdate(ACTION_DATA_RECEIVED);
                }
                if (uuid.equalsIgnoreCase(STREAM_CHAR)){
                    final byte[] data = characteristic.getValue();

                    okunanData=new String(data);
                    broadcastUpdate(STRING_RECEIVED);
                }
                if (uuid.equalsIgnoreCase(STRING_CHAR)){
                    final byte[] data = characteristic.getValue();

                    okunanData=new String(data);
                    broadcastUpdate(STRING_RECEIVED);

                }
                if (uuid.equalsIgnoreCase(DATA_SERVICE)){
                    final byte[] data = characteristic.getValue();

                    okunanData=new String(data);
                    broadcastUpdate(STRING_RECEIVED);
                }


                // Yeni etkinliğin mevcut olduğunu ana etkinliğe bildirin

            }
        }

    };


    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {

        final Intent intent = new Intent(action);

        if ((UUID.fromString(BUTTON0_STATE)).equals(characteristic.getUuid())) {
            // State of button 0 has changed. Add id and value to broadcast
            intent.putExtra(EXTRA_BUTTON0, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,0));
        }
        else if ((UUID.fromString(BUTTON1_STATE)).equals(characteristic.getUuid())) {
            // State of button 1 has changed. Add id and value to broadcast
            intent.putExtra(EXTRA_BUTTON1, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,0));
        }
        else if ((UUID.fromString(LED0_STATE)).equals(characteristic.getUuid())) {
            // State of led 0 has changed. Add id and value to broadcast
            intent.putExtra(EXTRA_LED0, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,0));
        }
        else if ((UUID.fromString(LED1_STATE)).equals(characteristic.getUuid())) {
            // State of led 1 has changed. Add id and value to broadcast
            intent.putExtra(EXTRA_LED1, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,0));
        }
        else {
            // Write the data formatted as a string
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                intent.putExtra(EXTRA_DATA, new String(data));
            }
        }

        sendBroadcast(intent);
    }



}