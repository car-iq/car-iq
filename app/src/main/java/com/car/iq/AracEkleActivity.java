package com.car.iq;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.CaseMap;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class AracEkleActivity extends AppCompatActivity {

    EditText plaka_edittext;
    EditText marka_edittext;
    EditText model_edittext;
    EditText yil_edittext;
    EditText tsmail_edittext;
    TextView plaka_hata;
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arac_ekle);

        db=new DatabaseHelper(this);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        plaka_edittext=findViewById(R.id.plaka_edittext);
        marka_edittext=findViewById(R.id.marka_edittext);
        model_edittext=findViewById(R.id.model_edittext);
        yil_edittext=findViewById(R.id.yil_edittext);
        tsmail_edittext=findViewById(R.id.tsmail_edittext);
        plaka_hata=findViewById(R.id.plaka_hata);


    }

    public void arac_kaydet(View view)
    {
        String marka,model,tsmail;
        int yil;
        if(yil_edittext.getText().toString().isEmpty()){
            yil=0;
        }
        else{
            yil=Integer.parseInt(yil_edittext.getText().toString());
        }

        if(plakaKontrol())
        {
            if(db.insertArac(plaka_edittext.getText().toString(),marka_edittext.getText().toString(),model_edittext.getText().toString(),yil,tsmail_edittext.getText().toString())){
                this.finish();
                Toast.makeText(this,"Giriş Başarılı",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, AracBilgisiActivity.class));
                this.finish();

            }
            else{
                Toast.makeText(this,"Veri Eklenemedi",Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            plaka_hata.setVisibility(View.VISIBLE);
        }
    }

    public boolean plakaKontrol()
    {
        String plaka=plaka_edittext.getText().toString();
        if(db.plakaZatenVar(plaka)){
            plaka_hata.setText("Bu plaka ile kayıtlı araç mevcut!");
            plaka_hata.setVisibility(View.VISIBLE);
            return false;
        }
        else if(plaka.length()<6){
            return false;
        }
        else if(kucukVarMi(plaka)){
            plaka_hata.setText("Tüm harfleri büyük karakter girin!");
            plaka_hata.setVisibility(View.VISIBLE);
            return false;
        }
        else if (Character.isDigit(plaka.charAt(0))&&Character.isDigit(plaka.charAt(1))&&Character.isLetter(plaka.charAt(2))&&plaka.length()<=8 && karakterKontrol(plaka))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean karakterKontrol(String plaka){
        String karakterler="ABCDEFGHIJKLMNOPRSTUVYZ1234567890";
        for(int i=0;i<plaka.length();i++){
            if (!karakterler.contains(String.valueOf(plaka.charAt(i)))) {
             return false;
            }
        }
        return true;
    }

    public boolean kucukVarMi(String plaka){
        String karakterler="abcdefghıijklmnoprstuvyz";
        for(int i=0;i<karakterler.length();i++) {
            if (plaka.contains(String.valueOf(karakterler.charAt(i))))
            {
                return true;
            }
        }
        return false;
    }
}
