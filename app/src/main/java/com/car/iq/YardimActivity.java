package com.car.iq;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class YardimActivity extends AppCompatActivity {

    SessionManagement session;
    DatabaseHelper db;
    String plaka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yardim);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());
        plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);

    }

    public void ana_sayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }


    }
    public void makeToast(){
        Toast.makeText(YardimActivity.this,"Veriler Eklendi",Toast.LENGTH_SHORT);
    }

    public static String getCalculatedDate(int days) {
        String dateFormat="yyyy-MM-dd";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }
}
