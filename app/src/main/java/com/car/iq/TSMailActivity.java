package com.car.iq;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class TSMailActivity extends AppCompatActivity {

    TextView plakaText;
    TextView co2;
    TextView co;
    TextView o2;
    TextView nox;
    TextView tariharaligi;
    DatabaseHelper db;
    SessionManagement session;
    ScrollView scrollView;
    String plaka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_s_mail);

        scrollView=findViewById(R.id.scroll);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        co2=findViewById(R.id.co2_value);
        o2=findViewById(R.id.o2_value);
        co=findViewById(R.id.co_value);
        nox=findViewById(R.id.nox_value);
        tariharaligi=findViewById(R.id.tarihAraligi);
        plakaText=findViewById(R.id.plaka_value);


        plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);

        co2.setText(db.ortalamaAl("co2",plaka));
        o2.setText(db.ortalamaAl("o2",plaka));
        co.setText(db.ortalamaAl("co",plaka));
        nox.setText(db.ortalamaAl("nox",plaka));

        plakaText.setText(plaka);
        tariharaligi.setText(db.tarihAraligiAl(plaka));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

    }
    public void gonderButon(View view)
    {
        writeToFile(db.mailVeriler(plaka),TSMailActivity.this);
        DownloadBitMap(scrollView);
        sendEmail();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                anasayfa_buton();
            }
        }, 4000);   //5 seconds
    }
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }
    public void anasayfa_buton()
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public void DownloadBitMap(ScrollView iv)
    {
        ImageGenerator ig=new ImageGenerator();
        ig.downloadData(iv);
        Log.e("callPhone: ", "permission" );
    }

    public void sendEmail(){
        Mail mail =new Mail(this);
        mail.message="Teknik Servis Uzmanının Dikkatine\n"+
                plaka +"plakalı aracın emisyon verisi ekte EXCEL ile açabileceğiniz şekilde bilginize sunulmuştur. Hızlı bir bilgi edinme amacına yönelik olarak son 30 ölçümün grafiği de ayrıca aşağıda verilmiştir.";
        mail.subject="BAKAS BILISIM Car-IQ ver.1.0 "+plaka+" plakalı aracın emisyon verisi:";
        String tsmail=db.getTSMail(session.getPlaka().get(SessionManagement.KEY_PLAKA));
        if(tsmail.isEmpty()||tsmail==""){
            tsmail="info@compositeware.com";
        }
        mail.email=tsmail;
        mail.execute();

    }


    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("Veriler.csv", Context.MODE_PRIVATE),"windows-1254");
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "Dosya yazması başarısız: " + e.toString());
        }
    }

}